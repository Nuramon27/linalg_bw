/* The black_walker_logic library contains the program logic for
 * Black Walker, which offers a graphical interface for writing a dream diary.
 * Copyright 2017-2018  Manuel Simon
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>. */

pub mod polynom;
#[cfg(test)]
mod tests;

use std::iter::Sum;
use std::cmp::PartialOrd;
use std::ops::{Mul, Div, Neg};
use std::fmt::{Debug, Display};

use num_traits::{Zero, One, Float, Num, NumAssign};
use ndarray::s;
use ndarray::array;
use ndarray::{ArrayBase, Axis, Array1, Array2, Ix1, Ix2, Data, DataMut};
use noisy_float::types::R32;

use polynom::{Abs, Polynom};

#[allow(dead_code)]
const NULL_BOUND: f32 = 1e-5f32;
const MAX_ITERATIONS: usize = 0x100;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ConvergenceError<T> {
    Impossible(String),
    MaxIterations(T),
    Divergence(T),
}

impl<T: Debug> Display for ConvergenceError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self {
            ConvergenceError::Impossible(s) => write!(f, concat!(
                "Algorithm cannot converge under given conditions",
                "Error: {}"
            ), s),
            ConvergenceError::MaxIterations(v) => {
                write!(f, concat!(
                    "Maximum number of Iterations ({}) exceeded\n",
                    "Last approximative result: {:?}",
                ), crate::MAX_ITERATIONS, v)
            },
            ConvergenceError::Divergence(v) => {
                write!(f, concat!(
                    "Algorithm diverges for given starting point\n",
                    "Last approximative result: {:?}",
                ), v)
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum NumericError {
    Impossible(String),
    Conv(String),
}

impl Display for NumericError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self {
            NumericError::Impossible(s) => write!(f, concat!(
                "Algorithm cannot converge under given conditions",
                "Error: {}"
            ), s),
            NumericError::Conv(v) => {
                write!(f, concat!(
                    "Subroutine did not converge.\n",
                    "Error: {}",
                ), v)
            },
        }
    }
}

impl<T: Debug> From<ConvergenceError<T>> for NumericError {
    fn from(other: ConvergenceError<T>) -> Self {
        NumericError::Conv(other.to_string())
    }
}

fn householder_reflection<S1, S2>(reflector: &ArrayBase<S1, Ix1>, v: &ArrayBase<S2, Ix1>) -> Array1<f32>
where
    S1: Data<Elem = f32>,
    S2: Data<Elem = f32>,
{
    let mut res = v.to_owned();
    res -= &(2.0 * reflector.dot(v) * reflector);
    res
}

fn householder_reflection_mat<S1, S2>(reflector: &ArrayBase<S1, Ix1>, v: &ArrayBase<S2, Ix2>) -> Array2<f32>
where
    S1: Data<Elem = f32>,
    S2: Data<Elem = f32>,
{
    let mut res = v.to_owned();
    let reflector_mat = reflector.to_owned().into_shape([reflector.shape()[0], 1]).unwrap();
    res -= &(2.0 * reflector_mat.dot(&reflector_mat.t().dot(v)));
    res
}

fn householder_orthogonalization<S1: Data<Elem = f32>>(x: &ArrayBase<S1, Ix1>, j: usize) -> Array1<f32> {
    let mut z = x.to_owned();
    //notprintln!("x_{}: {:?}", j, z);
    z.slice_mut(s![0..j]).fill(0.0);
    let beta: f32 = z.iter().map(|a| a*a).sum::<f32>().sqrt() * z[j].signum();
    //notprintln!("beta {}", beta);
    z[j] += beta;
    //notprintln!("z_{}: {:?}", j, z);
    z /= (2.0 * beta * z[j]).sqrt();
    //notprintln!("w_{}: {:?}", j, z);
    z
}

/// ## Panics
/// If the matrix is empty or not squared.
fn char_poly_hessenberg<S: Data<Elem=f32>>(mat: &ArrayBase<S, Ix2>) -> Polynom<f32> {
    assert_ne!(mat.shape(), &[0, 0], "Matrix must not be empty!");
    assert_eq!(mat.shape()[0], mat.shape()[1], "Matrix must be squared!");

    let mut res = Polynom::new(&[-mat[[0, 0]], 1.0]);

    let mut lastres = res.clone();
    let mut forelastres = Polynom::one();
    for i in 1..mat.shape()[0] {
        res *= Polynom::new(&[-mat[[i, i]], 1.0]);
        res -= forelastres.clone()*mat[[i-1, i]]*mat[[i, i-1]];
        forelastres = lastres;
        lastres = res.clone();
    }

    res
}

fn plane_rotation<S: DataMut<Elem=T>, T: 'static + Copy + Zero + One + Num + Neg<Output=T>>(mat: &mut ArrayBase<S, Ix2>, c: T, s: T, i: usize) {
    let new_part = &array![
        [c,  s],
        [-s, c]
    ].dot(&mat.slice(s![i..(i+2), ..]));

    mat.slice_mut(s![i..(i+2), ..]).assign(new_part);
}

fn solve_homogenous_hessenberg<S: Data<Elem=f32>>(mat: &ArrayBase<S, Ix2>) -> Vec<Array1<f32>> {
    let mut mat = mat.to_owned();
    for i in 0..(mat.shape()[0]-1) {
        let norm = (mat[[i, i]].powi(2) + mat[[i+1, i]].powi(2)).sqrt();
        let (c, s) = (mat[[i, i]]/norm, mat[[i+1, i]]/norm);
        plane_rotation(&mut mat, c, s, i);
    }

    let mut res = Vec::new();
    for i in (0..mat.shape()[0]-1).rev() {
        if !res.is_empty() && mat[[i, i]]/mat.slice(s![..i, i])
            .iter()
            .filter_map(|a| R32::try_new(*a))
            .max()
            .map(R32::raw)
            .unwrap_or(1.0) < NULL_BOUND {
            break;
        }
        res.push(solve_upper_triangular(&mat.slice(s![..i, ..i]), mat.slice(s![..i+1, i]).to_owned()));
    }
    res
}

fn solve_upper_triangular<S1: Data<Elem=T>, S2: DataMut<Elem=T>, T: std::fmt::Debug+ Copy + Num + NumAssign>(mat: &ArrayBase<S1, Ix2>, b: ArrayBase<S2, Ix1>) -> Array1<T> {
    let mut b = b.to_owned();
    for (i, mt) in mat.axis_iter(Axis(1)).enumerate().rev() {
        let pivot = mt[[i]];
        b[[i]] /= pivot;
        let b_start = b[[i]];
        for (mtt, bt) in (mt.slice(s![0..i]).into_iter().zip(b.iter_mut())).rev() {
            *bt -= b_start * (*mtt);
        }
    }

    b
}

fn cluster_eigenvalues<T: Copy + Abs<Output=R> + Num, R: PartialOrd>(mut eig: Vec<T>, prec: R) -> Vec<Vec<T>> {
    let mut res: Vec<Vec<T>> = Vec::new();
    while let Some(curr_val) = eig.pop() {
        let mut neighbor_found = false;
        for eig_group in &mut res {
            for &mut val in eig_group.iter_mut() {
                if ((curr_val - val)/(curr_val + val)).abs() < prec {
                    neighbor_found = true;
                    break;
                }
            }
            if neighbor_found {
                eig_group.push(curr_val);
                break;
            }
        }
        if !neighbor_found {
            res.push(vec![curr_val]);
        }
    }

    res
}

fn complete_plane_rotations<S: DataMut<Elem=T>, T, R>(mat: &mut ArrayBase<S, Ix2>) -> Vec<(T, T)>
where T: 'static + Copy + Abs<Output=R> + Num + NumAssign + Mul<R, Output=T> + Div<R, Output=T> + Neg<Output=T>,
    R: Sum + Float + PartialOrd + Mul<T, Output=T>,  {
    let mut rotation_params = Vec::new();
    for i in 0..(mat.shape()[0]-1) {
        let norm = ((mat[[i, i]]*mat[[i, i]]).abs() + (mat[[i+1, i]]*mat[[i+1, i]]).abs()).sqrt();
        let (c, s) = (mat[[i, i]]/norm, mat[[i+1, i]]/norm);
        plane_rotation(mat, c, s, i);
        rotation_params.push((c, s));
    }
    rotation_params
}

fn inverse_approx<
    T: std::fmt::Debug + 'static + Copy + Abs<Output=R> + Num + NumAssign + Mul<R, Output=T> + Div<R, Output=T> + Neg<Output=T>,
    R: std::fmt::Debug + Sum + Float + PartialOrd + Mul<T, Output=T>,
    S1: Data<Elem=T>,
    S2: Data<Elem=T>,
>(mat: &ArrayBase<S1, Ix2>, eig: &Vec<Vec<T>>, start: ArrayBase<S2, Ix1>, prec: R) -> Array2<T> {
    let mut res = Array2::zeros([mat.shape()[0], mat.shape()[0]]);
    let mut eig_count = 0;
    for eig_group in eig {
        let mut eig_vec: Vec<Array1<T>> = Vec::new();
        for &eig_val in eig_group {
            let mut eigmat = -mat.to_owned();
            for d in eigmat.diag_mut() {
                *d += eig_val* (R::one() + (R::one() + R::one() + R::one())*prec);
            }
            let eigmatclone = eigmat.clone();
            let rotation_params = complete_plane_rotations(&mut eigmat);
            let mut try_vec = start.to_owned().insert_axis(Axis(1));
            ////notprintln!("eigmat: {:?}", eigmat);
            for _ in 0..4 {
                for (i, &(c, s)) in rotation_params.iter().enumerate() {
                    plane_rotation(&mut try_vec, c, s, i);
                }
                ////notprintln!("try_vec_before: {:?}", try_vec);
                try_vec = solve_upper_triangular(&eigmat, try_vec.slice(s![.., 0]).to_owned()).insert_axis(Axis(1));
                ////notprintln!("try_vec_after: {:?}", try_vec);
                ////notprintln!("probe: {:?}", eigmat.dot(&try_vec));
                for it in &eig_vec {
                    try_vec -= &(it.dot(&try_vec) * it)
                }
                let norm = try_vec.iter().map(|a| a.abs()*a.abs()).sum::<R>().sqrt();
                ////notprintln!("norm: {:?}", norm);
                for it in try_vec.iter_mut() {
                    *it = (*it)/norm;
                }
            }
            eig_vec.push(try_vec.slice(s![.., 0]).to_owned());
        }
        for it in eig_vec {
            res.slice_mut(s![.., eig_count]).assign(&it);
            eig_count += 1;
        }
    }
    res
}

pub fn eigenvec_arnoldi_symmetric(mat: &Array2<f32>, start: &Array1<f32>, m: usize)
    -> Result<(Array1<f32>, Array2<f32>), NumericError> {
    // N
    let dim = start.shape()[0];
    // N
    let mut z = start/start.iter().map(|a| a*a).sum::<f32>().sqrt();
    // N x m
    let mut w = Array2::zeros([dim, m+1]);
    // N x m
    let mut h = Array2::zeros([dim, if m+1 == dim { dim } else { m }]);
    let mut q = Array2::zeros([dim, m+1]);
    for j in 0..m+1 {
        // N                                                                N
        w.index_axis_mut(Axis(1), j).assign(&householder_orthogonalization(&z, j));
        if 1 <= j {
            h.index_axis_mut(Axis(1), j-1).assign(&householder_reflection(
                &w.index_axis(Axis(1), j), &z
            ));
        }
        let mut v = Array1::zeros([dim]);
        v[[j]] = 1.0;
        for l in (0..j+1).into_iter().rev() {
            v = householder_reflection(
                &w.index_axis(Axis(1), l), &v
            );
        }
        q.index_axis_mut(Axis(1), j).assign(&v);
        v = mat.dot(&v);
        for l in 0..j+1 {
            v = householder_reflection(
                &w.index_axis(Axis(1), l), &v
            );
        }
        let norm = v.iter().map(|a| a*a).sum::<f32>().sqrt();
        z = v;

        if j+1 == dim {
            h.index_axis_mut(Axis(1), j).assign(&z);
        }
    }
    let final_dim = if m+1 == dim { dim } else { m };
    let h_sym = (h.slice(s![0..final_dim, 0..final_dim]).to_owned() + h.slice(s![0..final_dim, 0..final_dim]).t())/2.0;
    //notprintln!("h_sym: {:?}", h_sym);
    let char_poly = char_poly_hessenberg(&h_sym);
    //notprintln!("char_poly: {:?}", char_poly);
    let eigenval = char_poly.solve_durand_real(1e-4)?;
    // Dangger of Panic!
    let eigenval = cluster_eigenvalues(eigenval, 1e-3);
    //notprintln!("cluster: {:?}", eigenval);
    let start: Array1<f32> = Array1::ones([final_dim]);
    let res = inverse_approx(&h_sym, &eigenval, start, 1e-3);


    //notprintln!("res: {:?}", res);
    //notprintln!("test: {:?}", h_sym.dot(&res));
    //notprintln!("q: {:?}", q);
    let mut eigenval_final = Vec::new();
    for it in eigenval {
        eigenval_final.extend(it);
    }
    Ok((Array1::from_vec(eigenval_final), q.slice(s![.., ..final_dim]).dot(&res)))
}
