use std::ops::{Mul, DivAssign};
use std::iter::Sum;

use num_traits::{Float, FloatConst, Num, NumAssign, NumCast};
use num_complex::Complex;

use crate::ConvergenceError;

use super::{Polynom, Abs, Ring};

impl<T: Ring> Polynom<T> {
    /// Finds all roots of the Polynom in the field of values of type `U`.
    ///
    /// The roots are searched by the durand-kerner-algorithm. You have to provide
    /// a vector of starting points `start`, whose size must match
    /// the degree of the polynomial. Since the durand-kerner-algorithm
    /// has guaranteed global convergence (up to starting points with a symmetry
    /// that the zeros do not have), the choice of start is not too important
    /// and only affects convergence veloicity.
    ///
    /// The parameter `prec` determines the convergence-condition.
    /// Convergence is assumed when the value of the polynomial at all candidates
    /// for zeros is below `prec*bound`, where `bound` is the maximum of the absolute values
    /// of the normalized coefficients of the polynomial plus one.
    ///
    /// For finding roots in the set of real numbers,
    /// [`solve_durand_real`](Polynom::solve_durand_real) chooses a set of starting points
    /// according to an estimation of the range in wich the zeros lie.
    ///
    /// For finding roots in the set of complex numbers,
    /// [`solve_durand_complex`](Polynom::solve:durand_complex) does the same
    /// in the complex plane.
    pub fn solve_durand_start<U, R>(&self, start: Vec<U>, prec: R)
        -> Result<Vec<U>, ConvergenceError<Vec<U>>>
    where
        T: Abs<Output=R>,
        U: Ring + From<T> + Abs<Output=R> + Num + NumAssign + Mul<T, Output=U> + DivAssign<T>,
        R: Copy + PartialOrd + NumAssign {
        if self.grad() <= 0 {
            return Err(ConvergenceError::Impossible(
                "Attempt to find root of constant polynomial.".to_string()
            ));
        }
        if start.len() != self.grad() as usize {
            panic!("The length of start must exactly match the degree!");
        }

        let mut bound = R::zero();
        for it in self.c[..self.c.len()-1].iter()
            .map(|a| a.abs()) {
            if it > bound {
                bound = it
            }
        }
        bound /= self.c.last().unwrap().abs();
        bound += R::one();

        let leading_coeff = *self.c.last().unwrap();
        let mut restick = start;
        let mut further = true;
        let mut i = 0;
        while further {
            further = false;
            for i in 0..restick.len() {
                let tock = restick[i];
                let mut v = self.eval(tock);
                v /= leading_coeff;
                if v.abs()/bound > prec {
                    further = true;
                }
                for (j, tick) in restick.iter().enumerate() {
                    if i != j {
                        v /= tock - *tick;
                    }
                }
                *restick.get_mut(i).unwrap() -= v;
            }
            i += 1;
            if i > crate::MAX_ITERATIONS {
                return Err(ConvergenceError::MaxIterations(restick));
            }
        }

        Ok(restick)
    }
}

impl<T: Ring> Polynom<T> {
    /// Finds all roots of the polynomial in the set of real numbers.
    ///
    /// The algorithm will only converge for a polynomial of degree `n`
    /// if it has `n` real roots.
    ///
    /// The parameter `prec` determines the convergence-condition.
    ///
    /// For a detailed explanation of all parameters see
    /// [`solve_durand_start`](Polynom::solve_durand_start).
    pub fn solve_durand_real<U, R>(&self, prec: R)
        -> Result<Vec<U>, ConvergenceError<Vec<U>>>
    where
        T: Abs<Output=R>,
        U: Copy + From<T> + Abs<Output=R> + Float + NumAssign + Sum +
           Mul<T, Output=U> + Mul<R, Output=U> + DivAssign<T>,
        R: Copy + PartialOrd + NumAssign {
        if self.grad() <= 0 {
            panic!("Attempt to find root of constant polynomial!");
        }
        let mut bound = R::zero();
        for it in self.c[..self.c.len()-1].iter()
            .map(|a| a.abs()) {
            if it > bound {
                bound = it
            }
        }
        bound /= self.c.last().unwrap().abs();
        bound += R::one();

        let grad: U = match NumCast::from(self.grad() as usize) {
            Some(v) => v,
            None => (0..self.grad()).into_iter().map(|_| U::one()).sum(),
        };
        let mut start = Vec::with_capacity(self.grad() as usize);
        let mut j = U::one();
        for _ in 0..(self.grad() as usize) {
            j += U::one();
            start.push(j/grad*bound - (U::one() - j/grad)*bound);
        }
        self.solve_durand_start(start, prec)
    }
}


impl<T: Ring> Polynom<T> {
    /// Finds all roots of the polynomial in the set of complex numbers.
    ///
    /// The algorithm will converge for any polynomial with a degree of
    /// at least 2.
    ///
    /// The parameter `prec` determines the convergence-condition.
    ///
    /// For a detailed explanation of all parameters see
    /// [`solve_durand_start`](Polynom::solve_durand_start).
    pub fn solve_durand_complex<U>(&self, prec: U)
        -> Result<Vec<Complex<U>>, ConvergenceError<Vec<Complex<U>>>>
    where
        T: Abs<Output=U>,
        U: Copy + NumAssign + Float + FloatConst + Sum,
        Complex<U>: From<T> + DivAssign<T> + Mul<T, Output = Complex<U>> {
        if self.grad() <= 0 {
            panic!("Attempt to find root of constant polynomial!");
        }
        let mut bound = U::zero();
        for it in self.c[..self.c.len()-1]
            .iter()
            .map(|a| a.abs()) {
            if it > bound {
                bound = it
            }
        }
        bound /= Complex::<U>::norm(std::convert::From::<T>::from(*self.c.last().unwrap()));
        bound += U::one();

        let grad: U = match NumCast::from(self.grad() as usize) {
            Some(v) => v,
            None => (0..self.grad()).into_iter().map(|_| U::one()).sum(),
        };
        let mut start = Vec::with_capacity(self.grad() as usize);
        let mut j = U::one();
        for _ in 0..(self.grad() as usize) {
            j += U::one() + U::one() + U::one() + U::one();
            start.push(Complex::<U>::from_polar(
                bound,
                j * (U::FRAC_PI_2()/grad)
            ))
        }
        self.solve_durand_start(start, prec)
    }
}

#[cfg(test)]
mod tests {
    use num_complex::Complex;
    use num_traits::One;

    use crate::assert_float_relative_eq_m;

    use super::super::Polynom;

    #[test]
    fn durand_kerner_simple() {
        let p = Polynom::new(&[-1.0f32, 2.0]);
        assert_float_relative_eq_m!(
            p.solve_durand_start(vec![0.1f32], 1.0e-3f32)
                .expect("No solution found")
                .first().copied().unwrap(), 0.5f32,
            1.0e-3f32
        );
    }

    #[test]
    fn durand_kerner_three() {
        let mut p = Polynom::new(&[-2.0f32, 1.0]);
        p *= Polynom::new(&[1.0f32, 1.0]);
        p *= Polynom::new(&[-4.0f32, 1.0]);
        let sol = p.solve_durand_start(vec![-5.0, 0.0, 5.0], 1e-3)
            .expect("No solution found");
        let truesol = [-1.0, 2.0, 4.0];
        for &it in &sol {
            let mut correct = false;
            for &jt in &truesol {
                if ((it - jt)/jt).abs() < 1e-3 {
                    correct = true;
                }
            }
            if !correct {
                panic!(concat!(
                    "Solution not found\n",
                    "Found solutions: {:?}\n",
                    "True solutions: {:?}\n"
                ), sol, truesol);
            }
        }
    }

    #[test]
    fn durand_kerner_complex() {
        let mut p = Polynom::new(&[-Complex::new(2.0f32, 3.0), Complex::one()]);
        p *= Polynom::new(&[-Complex::new(-1.0f32, 0.5), Complex::one()]);
        p *= Polynom::new(&[-Complex::new(3.0f32, -3.0), Complex::one()]);
        p *= Polynom::new(&[-Complex::new(0.0f32, 1.0), Complex::one()]);
        let sol = p.solve_durand_complex(1e-3)
            .expect("No solution found");
        let truesol = [
            Complex::new(2.0, 3.0), Complex::new(-1.0, 0.5),
            Complex::new(3.0, -3.0), Complex::new(0.0, 1.0),
        ];
        for &it in &sol {
            let mut correct = false;
            for &jt in &truesol {
                if ((it - jt)/jt).norm() < 1e-3 {
                    correct = true;
                }
            }
            if !correct {
                panic!(concat!(
                    "Solution not found\n",
                    "Found solutions: {:?}\n",
                    "True solutions: {:?}\n"
                ), sol, truesol);
            }
        }
    }
}