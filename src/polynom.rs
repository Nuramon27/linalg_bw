pub mod durand_kerner;

use std::ops::{Add, AddAssign, Sub, SubAssign, Neg, Mul, MulAssign, Div, DivAssign};
use std::convert::{TryFrom};
use std::iter::FromIterator;

use num_traits::{Zero, One, Float};
use num_complex::Complex;

/// A collection of traits every type must implement to be used as the base
/// type for a `Polynom`.
pub trait Ring: Copy + Zero + One + AddAssign<Self> + Mul<Self, Output=Self> {}

impl<T> Ring for T
    where T: Copy + Zero + One + AddAssign<T> + Mul<T, Output=T> {}

pub trait Abs {
    type Output;
    fn abs(&self) -> Self::Output;
}

impl Abs for f32 {
    type Output = Self;
    fn abs(&self) -> Self {
        f32::abs(*self)
    }
}

impl Abs for f64 {
    type Output = Self;
    fn abs(&self) -> Self {
        f64::abs(*self)
    }
}

impl<T: Clone + Float> Abs for Complex<T> {
    type Output = T;
    fn abs(&self) -> Self::Output {
        self.norm()
    }
}

/// A Poynom over the ring of type `T`.
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
pub struct Polynom<T: Ring> {
    c: Vec<T>
}

impl<T: Ring> Polynom<T> {
    /// Constructs a new polynom with `coefficients` as
    /// coefficients.
    ///
    /// The coefficients are interpreted from low powers
    /// to high ones, i.e. the first coefficients is the constant term,
    /// and the last one the leading coefficient.
    pub fn new(coefficients: &[T]) -> Self {
        let mut res = Self {
            c: Vec::from(coefficients)
        };
        res.trunc_zero();
        res
    }

    /// Returns the degree of the polynom.
    ///
    /// For a zero polynom, `-1` is returned.
    pub fn grad(&self) -> isize {
        self.c.len() as isize - 1
    }

    /// Removes a tail of zero-coefficients.
    ///
    /// Internally, the degree of the polynom always directly coincides
    /// with the number of elements in [`c`](Polynom::c); this means
    /// that the last coefficient in `c` must always be non-zero.
    ///
    /// This function reensures this be removing all non-zero elements
    /// at the end of `c`.
    fn trunc_zero(&mut self) {
        while self.c.last().map(|a| a.is_zero()).unwrap_or(false) {
            self.c.pop();
        }
    }

    /// Convert `other` into another polynom by converting each
    /// coefficient via the `From`-trait.
    ///
    /// Usually, this should be an implementation of the `From`-trait,
    /// but this is not allowed due to coherence rules.
    ///
    /// *See also:*
    ///
    /// [`into`](Polynom::into)
    pub fn from<R: Ring>(other: Polynom<R>) -> Self where T: From<R> {
        Polynom {
            c: Vec::from_iter(other.c.into_iter().map(|a| T::from(a)))
        }
    }

    /// Convert `self` into another polynom by converting each
    /// coefficient via the `Into`-trait.
    ///
    /// Usually, this should be an implementation of the `Into`-trait,
    /// but this is not allowed due to coherence rules.
    ///
    /// *See also:*
    ///
    /// [`from`](Polynom::from)
    pub fn into<R: Ring>(self) -> Polynom<R> where T: Into<R> {
        Polynom {
            c: Vec::from_iter(self.c.into_iter().map(|a| T::into(a)))
        }
    }

    /// Evaluates the polynom at value `t`.
    pub fn eval<U: Ring + Mul<T, Output=U>>(&self, t: U) -> U {
        let mut power = U::one();
        let mut res = U::zero();
        for it in &self.c {
            res += power * (*it);
            power = power * t;
        }
        res
    }

    /// Iterate throgh the coefficients ordered by power,
    /// beginning with the constant term.
    pub fn iter(&self) -> std::slice::Iter<T> {
        self.into_iter()
    }
}

impl<T: Ring> Zero for Polynom<T> {
    fn is_zero(&self) -> bool {
        self.c.is_empty()
    }

    fn zero() -> Self {
        Polynom {
            c: Vec::new()
        }
    }
}

impl<T: Ring + One> One for Polynom<T> {
    fn one() -> Self {
        Polynom {
            c: vec![T::one()]
        }
    }
}

impl<T: Ring> IntoIterator for Polynom<T> {
    type Item = T;
    type IntoIter = std::vec::IntoIter<T>;
    fn into_iter(self) -> Self::IntoIter {
        self.c.into_iter()
    }
}

impl<'a, T: Ring> IntoIterator for &'a Polynom<T> {
    type Item = &'a T;
    type IntoIter = std::slice::Iter<'a, T>;
    fn into_iter(self) -> Self::IntoIter {
        self.c.iter()
    }
}

impl<T: Ring> FromIterator<T> for Polynom<T> {
    fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> Self {
        let mut res = Polynom {
            c: Vec::from_iter(iter)
        };
        res.trunc_zero();
        res
    }
}

impl<T: Ring> AddAssign<&Polynom<T>> for Polynom<T> {
    fn add_assign(&mut self, rhs: &Polynom<T>) {
        for (st, rt) in self.c.iter_mut().zip(&rhs.c) {
            *st += *rt
        }
        if rhs.c.len() > self.c.len() {
            self.c.extend_from_slice(&rhs.c[self.c.len()..]);
        }
        self.trunc_zero();
    }
}

impl<T: Ring> AddAssign<Polynom<T>> for Polynom<T> {
    fn add_assign(&mut self, rhs: Polynom<T>) {
        self.add_assign(&rhs);
    }
}

impl<T: Ring> Add<&Polynom<T>> for Polynom<T> {
    type Output = Self;
    fn add(mut self, rhs: &Polynom<T>) -> Self {
        self.add_assign(rhs);
        self
    }
}

impl<T: Ring> Add<Polynom<T>> for Polynom<T> {
    type Output = Self;
    fn add(self, rhs: Polynom<T>) -> Self {
        if self.c.len() >= rhs.c.len() {
            self.add(&rhs)
        } else {
            rhs.add(&self)
        }
    }
}

impl<T: Ring + SubAssign<T>> SubAssign<&Polynom<T>> for Polynom<T> {
    fn sub_assign(&mut self, rhs: &Polynom<T>) {
        for (st, rt) in self.c.iter_mut().zip(&rhs.c) {
            *st -= *rt
        }
        self.c.reserve(usize::try_from(rhs.grad()-self.grad()).unwrap_or(0));
        if rhs.c.len() > self.c.len() {
            for jt in &rhs.c[self.c.len()..] {
                let mut v = T::zero();
                v -= *jt;
                self.c.push(v);
            }
        }
        self.trunc_zero();
    }
}

impl<T: Ring + SubAssign<T>> SubAssign<Polynom<T>> for Polynom<T> {
    fn sub_assign(&mut self, rhs: Polynom<T>) {
        self.sub_assign(&rhs)
    }
}

impl<T: Ring + SubAssign<T>> Sub<&Polynom<T>> for Polynom<T> {
    type Output = Self;
    fn sub(mut self, rhs: &Polynom<T>) -> Self {
        self.sub_assign(rhs);
        self
    }
}

impl<T: Ring + SubAssign<T>> Sub<Polynom<T>> for Polynom<T> {
    type Output = Self;
    fn sub(self, rhs: Polynom<T>) -> Self {
        self.sub(&rhs)
    }
}

impl<T: Ring + Neg<Output=O>, O: Ring> Neg for Polynom<T> {
    type Output = Polynom<<T as Neg>::Output>;
    fn neg(self) -> <Self as Neg>::Output {
        (&self).neg()
    }
}

impl<'a, T: Ring + Neg<Output=O>, O: Ring> Neg for &'a Polynom<T> {
    type Output = <Polynom<T> as Neg>::Output;
    fn neg(self) -> <Self as Neg>::Output {
        Polynom::from_iter(self.iter().map(|a| a.neg()))
    }
}

impl<T: Ring> MulAssign<T> for Polynom<T> {
    fn mul_assign(&mut self, rhs: T) {
        if rhs.is_zero() {
            *self = Polynom::zero();
        } else {
            for it in &mut self.c {
                *it = (*it) * rhs;
            }
        }
    }
}

impl<T: Ring> MulAssign<&T> for Polynom<T> {
    fn mul_assign(&mut self, rhs: &T) {
        self.mul_assign(*rhs);
    }
}

impl<T: Ring> Mul<T> for Polynom<T> {
    type Output = Self;
    fn mul(mut self, rhs: T) -> Self {
        self.mul_assign(rhs);
        self
    }
}

impl<T: Ring> Mul<&T> for Polynom<T> {
    type Output = Self;
    fn mul(self, rhs: &T) -> Self {
        self.mul(*rhs)
    }
}

impl<T: Ring + Div<T, Output=T>> DivAssign<T> for Polynom<T> {
    fn div_assign(&mut self, rhs: T) {
        if rhs.is_zero() {
            *self = Polynom::zero();
        } else {
            for it in &mut self.c {
                *it = (*it) / rhs;
            }
        }
    }
}

impl<T: Ring + Div<T, Output=T>> DivAssign<&T> for Polynom<T> {
    fn div_assign(&mut self, rhs: &T) {
        self.div_assign(*rhs);
    }
}

impl<T: Ring + Div<T, Output=T>> Div<T> for Polynom<T> {
    type Output = Self;
    fn div(mut self, rhs: T) -> Self {
        self.div_assign(rhs);
        self
    }
}

impl<T: Ring + Div<T, Output=T>> Div<&T> for Polynom<T> {
    type Output = Self;
    fn div(self, rhs: &T) -> Self {
        self.div(*rhs)
    }
}

impl<T: Ring> MulAssign<&Polynom<T>> for Polynom<T> {
    fn mul_assign(&mut self, rhs: &Polynom<T>) {
        if rhs.is_zero() {
            *self = Self::zero();
            return;
        }

        self.c.reserve(usize::try_from(rhs.grad()).unwrap_or(0));
        for _ in 0..rhs.grad() {
            self.c.push(T::zero());
        }
        for i in (0..self.c.len()).into_iter().rev() {
            let selfval = std::mem::replace(&mut self.c[i], T::zero());
            for (it, jt) in self.c[i..].iter_mut().zip(rhs.c.iter()) {
                *it += selfval * (*jt);
            }
        }
    }
}

impl<T: Ring> MulAssign<Polynom<T>> for Polynom<T> {
    fn mul_assign(&mut self, rhs: Polynom<T>) {
        self.mul_assign(&rhs);
    }
}

impl<T: Ring> Mul<&Polynom<T>> for Polynom<T> {
    type Output = Self;
    fn mul(mut self, rhs: &Polynom<T>) -> Self {
        self.mul_assign(rhs);
        self
    }
}

impl<T: Ring> Mul<Polynom<T>> for Polynom<T> {
    type Output = Self;
    fn mul(self, rhs: Polynom<T>) -> Self {
        self.mul(&rhs)
    }
}

#[cfg(test)]
mod tests {
    use num_traits::{Zero};

    use super::Polynom;

    fn standard_polynom() -> Polynom<i16> {
        Polynom::new(&[5, -6, 2, 1, -2])
    }

    #[test]
    fn new() {
        let p = Polynom::new(&[1i16, 0, 0]);
        assert_eq!(p.c.len(), 1);
        assert_eq!(p.grad(), 0);
    }

    #[test]
    fn add() {
        let p = standard_polynom();
        assert_eq!(
            p.clone() + &Polynom::new(&[-2i16, 2, 4]),
            Polynom::new(&[3, -4, 6, 1, -2])
        );
        assert_eq!(
            p.clone() + &Polynom::new(&[-5i16, 0, 0, 0, 1, 3]),
            Polynom::new(&[0, -6, 2, 1, -1, 3])
        );
        assert_eq!(
            p + &Polynom::new(&[0i16, 0, 0, -1, 2]),
            Polynom::new(&[5, -6, 2])
        );
    }

    #[test]
    fn sub() {
        let p = standard_polynom();
        assert_eq!(
            p.clone() - &Polynom::new(&[-2i16, 2, 4]),
            Polynom::new(&[7, -8, -2, 1, -2])
        );
        assert_eq!(
            p.clone() - &Polynom::new(&[-5i16, 0, 0, 1, 3]),
            Polynom::new(&[10, -6, 2, 0, -5])
        );
        assert_eq!(
            p - &Polynom::new(&[0i16, 0, 0, 1, -2]),
            Polynom::new(&[5, -6, 2])
        );
    }

    #[test]
    fn sub_right_hand_shorter() {
        let p = standard_polynom();
        assert_eq!(
            p.clone() - &Polynom::new(&[-2i16, 2, 4]),
            Polynom::new(&[7, -8, -2, 1, -2])
        );
    }

    #[test]
    fn sub_right_hand_longer() {
        let p = standard_polynom();
        assert_eq!(
            p.clone() - &Polynom::new(&[-2i16, 2, 4, 0, 0, -1, 1]),
            Polynom::new(&[7, -8, -2, 1, -2, 1, -1])
        );
    }

    #[test]
    fn scalar_mul() {
        let p = standard_polynom();

        Polynom::new(&[5, -6, 2, 1, -2]);
        assert_eq!(
            p.clone() * 2i16,
            Polynom::new(&[10, -12, 4, 2, -4])
        );

        assert_eq!(
            p * 0i16,
            Polynom::new(&[])
        );
    }

    #[test]
    fn scalar_mul_zero() {
        let p = standard_polynom();
        assert!((p*0).is_zero());
    }

    #[test]
    fn mul() {
        let p = standard_polynom();
        assert_eq!(
            p.clone() * &Polynom::new(&[1, -2]),
            Polynom::new(&[5, -16, 14, -3, -4, 4])
        );
    }

    #[test]
    fn mul_zero() {
        let p = standard_polynom();
        assert!((p*Polynom::new(&[0, 0])).is_zero());
    }

    #[test]
    fn neg() {
        let p = standard_polynom();
        assert_eq!(
            -p,
            Polynom::new(&[-5, 6, -2, -1, 2])
        )
    }
}