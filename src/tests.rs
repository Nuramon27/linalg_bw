use ndarray::array;
use ndarray::Axis;

use super::polynom::Polynom;


#[macro_export]
macro_rules! assert_float_relative_eq_m {
    ($left:expr, $right:expr, $epsilon:expr) => ({
        let (left_val, right_val, epsilon) = ($left, $right, $epsilon);
        if ((left_val - right_val)/right_val).abs() > epsilon {
            panic!(concat!(
                "assertion failed in line {}: `(left == right)`\n",
                "   left: `{:?}`\n",
                "   right: `{:?}`\n"
            ), line!(), left_val, right_val,);
        }
    });
    ($left:expr, $right:expr, $epsilon:expr, $($arg:tt)+) => ({
        let (left_val, right_val, epsilon) = ($left, $right, $epsilon);
        if ((left_val - right_val)/right_val).abs() > epsilon {
            panic!(concat!(
                "assertion failed in line {}: `(left == right)`\n",
                "   left: `{:?}`\n",
                "   right: `{:?}`: {}\n"
            ), line!(), left_val, right_val, format_args!($($arg)+));
        }
    })
}

#[test]
fn char_poly_hessenberg_two_dim() {
    let mat = array![
        [1.0, -1.0,],
        [2.0,  2.0,],
    ];
    let char_poly = Polynom::new(&[4.0, -3.0, 1.0]);
    let char_poly_guess = super::char_poly_hessenberg(&mat);
    //notprintln!("char_poly: {:?}", char_poly_guess);
    assert_eq!(char_poly_guess.grad(), 2);
    for (i, (it, jt)) in char_poly.into_iter().zip(char_poly_guess.into_iter()).enumerate() {
        assert_float_relative_eq_m!(
            it, jt, 1e-3,
            "Wrong coefficient {}", i
        );
    }
}

#[test]
fn char_poly_hessenberg_three_dim() {
    let mat = array![
        [1.0, -1.0, 0.0],
        [2.0,  2.0, -3.0],
        [0.0, 2.0, 1.0],
    ];
    let char_poly = Polynom::new(&[-10.0, 13.0, -4.0, 1.0]);
    let char_poly_guess = super::char_poly_hessenberg(&mat);
    //notprintln!("char_poly: {:?}", char_poly_guess);
    assert_eq!(char_poly_guess.grad(), 3);
    for (i, (it, jt)) in char_poly.into_iter().zip(char_poly_guess.into_iter()).enumerate() {
        assert_float_relative_eq_m!(
            it, jt, 1e-3,
            "Wrong coefficient {}", i
        );
    }
}

#[test]
fn char_poly_hessenberg() {
    let mat = array![
        [1.0, -1.0, 0.0, 0.0,  0.0],
        [2.0,  2.0, 3.0, 0.0,  0.0],
        [0.0, -1.0, 2.0, 1.0,  0.0],
        [0.0,  0.0, 1.0, 4.0, -2.0],
        [0.0,  0.0, 0.0, -3.0,  5.0],
    ];
    let char_poly = Polynom::new(&[-134.0f32, 262.0, -190.0, 71.0, -14.0, 1.0]);
    let char_poly_guess = super::char_poly_hessenberg(&mat);
    //notprintln!("char_poly: {:?}", char_poly_guess);
    assert_eq!(char_poly_guess.grad(), 5);
    for (i, (it, jt)) in char_poly.into_iter().zip(char_poly_guess.into_iter()).enumerate() {
        assert_float_relative_eq_m!(
            it, jt, 1e-3,
            "Wrong coefficient {}", i
        );
    }
}

#[test]
fn solve_upper_triangular() {
    let mat = array![
        [ 1.0f32, -1.0,  3.0,  2.0],
        [ 0.0   ,  4.0, -4.0, -1.0],
        [ 0.0   ,  0.0, -2.0,  3.0],
        [ 0.0   ,  0.0,  0.0,  5.0],
    ];
    let a = array![3.0f32, 2.0, -1.0, 4.0];
    let b = mat.dot(&a);
    //notprintln!("b: {:?}", b);
    let sol = super::solve_upper_triangular(&mat, b);
    //notprintln!("sol: {:?}", sol);
    for (i, (it, jt)) in sol.iter().zip(a.iter()).enumerate() {
        assert_float_relative_eq_m!(
            it, jt, 1e-3,
            "Wrong component {}", i
        );
    }
}

#[test]
fn arnoldi() {
    let mat = array![
        [1.0, 0.0, -1.0, 1.5, 3.0],
        [0.0, 2.0, 2.5, 0.0, 3.0],
        [-1.0, 2.5, 3.0, 0.0, 0.0],
        [1.5, 0.0, 0.0, 4.0, 0.0],
        [3.0, 3.0, 0.0, 0.0, 5.0],
    ];
    let (eigenval, eigenvec) = super::eigenvec_arnoldi_symmetric(
        &mat,
        &mat.dot(&mat.dot(&array![1.0, 1.0, 1.0, 1.0, 1.0])),
        4
    ).expect("Solution not found");

    let prec = 0.25;
    let mul = mat.dot(&eigenvec);
    //notprintln!("eigenval: {:?}", eigenval);
    //notprintln!("eigenvec: {:?};\ntest: {:?}", eigenvec, mul);
    for (i, ((test, eigvec), eigval)) in mul.axis_iter(Axis(1)).zip(eigenvec.axis_iter(Axis(1))).zip(eigenval.iter()).enumerate() {
        for (j, (it, jt)) in test.iter().zip(eigvec.iter()).enumerate() {
            if (it + eigval*jt).abs() > prec {
                assert_float_relative_eq_m!(
                    it, eigval*jt, prec,
                    "Wrong component {} in eigenvector {} (eigenvalue {})", j, i, eigval
                );
            }
        }
    }
}

#[test]
fn rem() {
    let x = 5.0f32;
    let a = 5i32;
    //notprintln!("float: div: {:.4e}; rem: {:.4e}", x / 2.0f32, x % 2.0f32);
    //notprintln!("int: div: {}; rem: {}", a / 2i32, a % 2i32);
}